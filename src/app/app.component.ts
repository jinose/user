import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'user-app';
  constructor(private router: Router) { }
  userHome() {
    this.router.navigate(["/userdirectory/user-home"]);
    console.log("user home");
  }
}
