import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {AuthGuardService} from '../shared/auth-guard.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;
  arrValue: number[] = [];
  viewField: boolean = false;
  constructor(private authGuardService:AuthGuardService,private router: Router) { }

  ngOnInit() {
    this.arrValue = [];
    this.myForm = new FormGroup({
      'arrayNum': new FormControl('', [Validators.pattern('^[0-9][.]?[0-9]*$'), Validators.maxLength(4)]),
      'value': new FormControl('', [Validators.required, Validators.maxLength(4)])
    });
  }

  onAdd(){debugger
    if (this.arrValue.length < 5 && this.myForm.controls.arrayNum.valid) {debugger
      if (this.myForm.controls.arrayNum.value)
        this.arrValue.push(this.myForm.controls.arrayNum.value);
      this.myForm.controls.arrayNum.reset();
    }if (this.arrValue.length === 5) {debugger
       this.viewField=true;
    }
  }

  onSubmit(){
    if (this.myForm.valid) {
     var total = this.arrValue.reduce((acc, val) => acc*1 + val*1,0);
     this.authGuardService.setValues(total, this.myForm.controls.value.value);
    }
  }

  getUserDetails(){
    this.router.navigate(["/userdirectory"]);
  }
}
