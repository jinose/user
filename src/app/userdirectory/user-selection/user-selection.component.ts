import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import {UserService} from '../shared/user.service';

import * as moment from 'moment';
import { SelectionModel } from '@angular/cdk/collections';
import {Router} from '@angular/router';

export interface users {
  userName: string;
  dob: string;
  phoneNumber: string;
}
@Component({
  selector: 'app-user-selection',
  templateUrl: './user-selection.component.html',
  styleUrls: ['./user-selection.component.scss']
})
export class UserSelectionComponent implements OnInit {

  users :any;
  displayedColumns = ['select','userName', 'dob', 'phoneNumber'];
  dataSource = new MatTableDataSource();
  selection = new SelectionModel(true, []);
  constructor(private userService:UserService,private router:Router) { }

  ngOnInit() {
    this.users=[];
    this.userService.getJSON().subscribe(data => {
      
      this.users=data.users;
      this.dataSource.data=this.users
      console.log(this.users)
  })
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  logSelection() {
    this.selection.selected.forEach(s => console.log(s.name));
  }

  // Filter ages based on values fetched from dropdowns.
  onAgeFilter(ageFilter: string) {

    this.selection.clear();

    if (ageFilter == 'minor')
      this.dataSource.data = this.users.filter(e => ((moment().diff(e.dob, 'years') <= 18)));
    else if (ageFilter == 'major')
      this.dataSource.data = this.users.filter(e => ((moment().diff(e.dob, 'years') >= 18) && Number((moment().diff(e.dob, 'years')) <= 56)));
    else if (ageFilter == 'elder')
      this.dataSource.data = this.users.filter(e => ((moment().diff(e.dob, 'years') >= 56)));
    else
      this.dataSource.data = this.users;
  }

  sendData() {

    if (this.selection.selected.length > 0)
      this.userService.saveUserData(this.selection.selected);
    else {
      alert("Select atleast one data to send!")
    }
  }

  back(){
     this.router.navigate([""]);
  }
}
