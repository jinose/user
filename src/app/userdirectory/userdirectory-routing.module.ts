import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserdirectoryComponent } from './userdirectory.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserSelectionComponent} from './user-selection/user-selection.component';
import {UserHomeComponent} from './user-home/user-home.component';

const routes: Routes = [
  {
      path: "",
      component: UserdirectoryComponent,
      children: [
          {
              path: "user-list",
              component: UserListComponent
          },
          {
              path: "user-selection",
              component: UserSelectionComponent
          },
          {
            path: "",
            component: UserHomeComponent
        }

      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserdirectoryRoutingModule { }
