export class UserData {
    constructor(

        public userName: string = null,
        public dob: string = null,
        public phoneNumber: string = null

    ) { }
}
