import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'agepipe'
})
export class AgepipePipe implements PipeTransform {

  transform(value: Date | moment.Moment): any {

    if (!value) { return "NA"; };

    moment().diff(value, 'years') + " Years"

    return Number.isNaN(moment().diff(value, 'years')) ? "NA" : moment().diff(value, 'years');
  }

}
