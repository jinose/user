import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { UserData } from '../shared/model/user-data';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userData: BehaviorSubject<UserData[]> = new BehaviorSubject<UserData[]>([]);
  users: UserData[] = new Array<UserData>();

  constructor(private http: HttpClient) {
  //   this.getJSON().subscribe(data => {
  //     console.log(data);
  // });
   }

  public getJSON(): Observable<any> {
    return this.http.get("../assets/users.json");
}

saveUserData(users: UserData[]) {debugger

  this.users = [];
  users.forEach(e => this.users.push(e));
  this.userData.next(this.users);
}
getSavedUserData(): Subject<UserData[]> {

  return this.userData;

}
}
