import { Component, OnInit } from '@angular/core';
import { UserData } from '../shared/model/user-data';
import {UserService} from '../shared/user.service';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users: UserData[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getSavedUserData().subscribe(data => this.users = data);
  }

}
