import { Injectable } from '@angular/core';
import { CanActivate,Router,ActivatedRouteSnapshot,RouterStateSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private router:Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //const currentUser = this.authenticationService.currentUserValue;
    if ((Number(localStorage.getItem("sum")) == Number(localStorage.getItem("value")))) {
        // logged in so return true
        return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['']);
    return false;
}

  public setValues(sum, value) {
    localStorage.setItem("sum", sum);
    localStorage.setItem("value", value);
    this.router.navigate(["/admin"]);
  }
}
