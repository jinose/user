import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from '../app/login/login.component';
import {AuthGuardService} from './shared/auth-guard.service';
 
const routes: Routes = [{ path: 'userdirectory', loadChildren: () => import('./userdirectory/userdirectory.module').then(m => m.UserdirectoryModule) }, { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),canActivate:[AuthGuardService] }
,{
  path: '',
  component: LoginComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
